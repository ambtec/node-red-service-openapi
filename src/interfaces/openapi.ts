export interface CustomConfiguration {
  ORCHESTRATOR_ENDPOINT: string;
  SOCKETIO_ENDPOINT: string;
  MAIN_PROJECT: string;
}
