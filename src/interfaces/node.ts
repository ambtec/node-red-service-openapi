declare var process: {
  env: {
    NODE_ENV: string;
    PRODUCTION: string;
    PROJECTS_PATH: string;
    MAIN_PROJECT_ID: string;
    SOCKETIO_ENDPOINT: string;
    ORCHESTRATOR_ENDPOINT: string;
  };
  on: Function;
  emit: Function;
};
interface Window {
  mozRequestAnimationFrame: any;
  webkitRequestAnimationFrame: any;
  msRequestAnimationFrame: any;
  mozCancelAnimationFrame: any;
}
