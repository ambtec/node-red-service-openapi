import "./interfaces/node";
import { CustomConfiguration } from "./interfaces/openapi";
import {
  NodeRedApp,
  EditorNodeProperties,
  EditorRED,
  NodeMessageInFlow,
} from "node-red";
import express, { Request, Response } from "express";
// @ts-ignore
import Log from "ambtec-logger";

interface NodeRedAppCustom extends NodeRedApp {
  nodes: any;
}

interface CustomEditorNodeProperties extends EditorNodeProperties {
  path: string;
}

interface SelectValue {
  path: string | null;
  method: string | null;
  topic: string | null;
}

import path from "path";
import fs from "fs";
import crypto from "crypto";

module.exports = (RED: NodeRedAppCustom): void => {
  "use strict";

  const PROD = "true" === process.env.PRODUCTION;
  const defaultSwaggerYaml = path.join(__dirname, "storage", "swagger.yaml");
  const projectsRoot = process.env.PROJECTS_PATH
    ? process.env.PROJECTS_PATH
    : "/app/projects";
  const orchestratorMainProjectKey = process.env.MAIN_PROJECT_ID
    ? process.env.MAIN_PROJECT_ID
    : "orchestrator-flows";
  const socketEndpoint = process.env.SOCKETIO_ENDPOINT
    ? process.env.SOCKETIO_ENDPOINT
    : "/socket.io";
  const mainProjectPath = path.join(projectsRoot, orchestratorMainProjectKey);
  const mainYamlEndpoint = "/doc/swagger.yaml";

  const sendFile = (res: any, filename: string): void => {
    // Use the right function depending on Express 3.x/4.x
    if (res.sendFile) {
      res.sendFile(filename);
    } else {
      res.sendfile(filename);
    }
  };

  const writeProjectFile = (filePath: string, fileBody: string): void => {
    if (PROD) return;
    fs.writeFileSync(filePath, fileBody, {
      encoding: "utf8",
      flag: "w",
      mode: 0o666,
    });
  };

  const readProjectFile = (filePath: string): string => {
    return fs.readFileSync(filePath, {
      encoding: "utf8",
      flag: "r",
    });
  };

  const getConfiguration = (): CustomConfiguration => {
    const admin = "true" === process.env.PRODUCTION ? "" : "/admin";
    const url = process.env.ORCHESTRATOR_ENDPOINT
      ? process.env.ORCHESTRATOR_ENDPOINT
      : "http://localhost:1880";
    const socket = process.env.SOCKETIO_ENDPOINT
      ? process.env.SOCKETIO_ENDPOINT
      : "/socket.io";
    return {
      ORCHESTRATOR_ENDPOINT: url + admin,
      SOCKETIO_ENDPOINT: socket,
      MAIN_PROJECT: orchestratorMainProjectKey,
    };
  };

  const getAllMatches = (
    string: string,
    regex: RegExp,
    index: number = 1
  ): string[] => {
    index || (index = 1); // default to the first capturing group
    var matches = [];
    var match;
    while ((match = regex.exec(string))) {
      matches.push(match[index]);
    }
    return matches;
  };

  const addSlashPrefix = (path: string): string => {
    return "/" === path[0] ? path : `/${path}`;
  };

  const upperCamelCase = (str: string): string => {
    return str.substr(0, 1).toUpperCase() + str.substr(1);
  };

  const addProjectPrefix = (path: string, projectPrefix: string): string => {
    // Skip socket endpoints
    if (0 === path.indexOf(socketEndpoint)) return path;
    // Check for project prefix
    const hit = path.indexOf(`${projectPrefix}/`);
    // If prefix does not exist or path does not start with prefix
    if (hit === -1 || hit > 0) {
      return `${projectPrefix}${path}`;
    }
    return path;
  };

  const addReferencePrefix = (
    yamlContent: string,
    yamlReferenceItems: any,
    projectPrefix: string
  ): string => {
    for (const yamlReferenceItem of yamlReferenceItems) {
      const referenceParts = yamlReferenceItem.split("/");
      const lastItem = referenceParts.length - 1;
      if (!referenceParts[lastItem]) continue;
      let newModuleName = referenceParts[lastItem];
      const hit = newModuleName.indexOf(upperCamelCase(projectPrefix));
      // If prefix is not in name or name does not start with prefix or name is not upper camel case
      if (0 !== hit) {
        newModuleName =
          upperCamelCase(projectPrefix) + upperCamelCase(newModuleName);
      }
      if (referenceParts[lastItem] !== newModuleName) {
        const regex = new RegExp("(/|\\s)" + referenceParts[lastItem], "g");
        yamlContent = yamlContent.replace(regex, "$1" + newModuleName);
      }
    }
    return yamlContent;
  };

  const updateYamlConfiguration = (project: string, data: any): void => {
    if (PROD) return;
    // Proceed only if not in the main project
    if (orchestratorMainProjectKey === project) return;

    const projectRoot = `${projectsRoot}/${project}`;
    const projectRootYaml = `${projectRoot}/swagger.yaml`;
    const projectRootPackage = `${projectRoot}/package.json`;

    try {
      const projectConfigRaw = readProjectFile(projectRootPackage);
      const projectConfig = projectConfigRaw
        ? JSON.parse(projectConfigRaw)
        : {};
      const projectPrefix = projectConfig["openapi-project-route"]
        ? projectConfig["openapi-project-route"]
        : project;
      let yamlContentNew = data.config;

      // Handle path prefix
      const regexYamlPaths = new RegExp("\\n\\s+(\\/[^\\s\\[\\(\\:]+)", "g");
      const yamlPaths = getAllMatches(yamlContentNew, regexYamlPaths);
      for (const yamlPath of yamlPaths) {
        let newYamlPath = yamlPath;
        newYamlPath = addSlashPrefix(newYamlPath);
        newYamlPath = addProjectPrefix(newYamlPath, "/" + projectPrefix);

        if (yamlPath !== newYamlPath) {
          yamlContentNew = yamlContentNew.replace(yamlPath, newYamlPath);
        }
      }

      const regexYamlReferences = new RegExp("(#[^']+)", "g");
      const yamlReferences = getAllMatches(yamlContentNew, regexYamlReferences);
      let yamlReferenceItems: any = {};
      for (const yamlReference of yamlReferences) {
        // Use object to remove duplicates
        yamlReferenceItems[yamlReference] = yamlReference;
      }

      yamlContentNew = addReferencePrefix(
        yamlContentNew,
        Object.keys(yamlReferenceItems),
        projectPrefix
      );

      const yamlContentOriginal = readProjectFile(projectRootYaml);
      const yamlContentOriginalHash = crypto
        .createHash("sha256")
        .update(yamlContentOriginal)
        .digest("hex");
      const yamlContentNewHash = crypto
        .createHash("sha256")
        .update(yamlContentNew)
        .digest("hex");
      if (yamlContentOriginalHash !== yamlContentNewHash) {
        Log.debug({
          breadcrumbs: [
            "openapi",
            "httpAdmin",
            "post",
            "updateYamlConfiguration",
          ],
          message: "A new file version was written",
          properties: {
            file: projectRootYaml,
            yamlContentOriginalHash: yamlContentOriginalHash,
            yamlContentNewHash: yamlContentNewHash,
          },
        });
        writeProjectFile(projectRootYaml, yamlContentNew);
      }
    } catch (error) {
      Log.error({
        breadcrumbs: [
          "openapi",
          "httpAdmin",
          "post",
          "updateYamlConfiguration",
        ],
        message: "Could not update yaml config",
        properties: {
          file: projectRootYaml,
          error: error,
        },
      });
    }
  };

  const serveStaticFile = (
    req: Request,
    res: Response,
    sourceFolder: string
  ): void => {
    const filename = path.join(__dirname, sourceFolder, req.params[0]);
    sendFile(res, filename);
  };

  const serveIndexFile = (
    req: Request,
    res: Response,
    sourceFolder: string,
    project: string,
    readonly: boolean
  ): void => {
    const config = getConfiguration();
    const filename = path.join(__dirname, sourceFolder, req.params[0]);
    const fileBody = fs.readFileSync(filename, { encoding: "utf8", flag: "r" });
    const regex = new RegExp("<script>(.|\n)*?</script>");
    const configUrl = mainYamlEndpoint;
    const url =
      config.ORCHESTRATOR_ENDPOINT +
      "/{project}/swagger.yaml".replace("{project}", project);
    const newContent = `<script>
window.onload = function() {
  const editor = SwaggerEditorBundle({
    dom_id: '#swagger-editor',
    layout: 'StandaloneLayout',
    presets: [
      SwaggerEditorStandalonePreset
    ],
    configUrl: '${configUrl}',
    url: '${url}',
    onComplete: function() {
      const unrequiredMenuOptions = ['File', 'Edit', 'Insert'];
      const menuItems = document.querySelectorAll('#swagger-editor .swagger-editor-standalone > .topbar > .topbar-wrapper > div');
      for (const menuItem of menuItems) {
        if (unrequiredMenuOptions.indexOf(menuItem.querySelector('.menu-item').innerText) > -1) {
          menuItem.parentNode.removeChild(menuItem);

        }
      }

      ${
        readonly
          ? `function handler(e) {
        if (e.target instanceof HTMLTextAreaElement && e.target.classList.contains('ace_text-input')) {
          e.stopPropagation();
          e.preventDefault();
        }
      }
      document.addEventListener('keydown', handler, true);`
          : ""
      }
    }
  });
}
</script>`;
    const cleanNewContent = newContent
      .replace(/[\n\r]\s+/g, "")
      .replace(/[\n\r]/g, "");
    const newFileBody = fileBody.replace(regex, cleanNewContent);
    // res.status(200).setHeader('Cache-Control', 'public, max-age=3600')
    res
      .status(200)
      .setHeader("Cache-Control", "public, max-age=0")
      .send(newFileBody);
  };

  const projectSetterGetterHandler = (project: string): void => {
    if (PROD) return;
    const projectPathYaml = `/${project}/swagger.yaml`;
    const projectRoot = `${projectsRoot}/${project}`;
    const projectRootYaml = `${projectRoot}/swagger.yaml`;
    const projectRootFlows = `${projectRoot}/flows.json`;

    if (!fs.existsSync(projectRootYaml)) {
      const yamlContent = readProjectFile(defaultSwaggerYaml);
      updateYamlConfiguration(project, {
        nodeId: null,
        option: "",
        config: yamlContent,
      });
    }

    RED.httpAdmin.get(projectPathYaml, (req: Request, res: Response): void => {
      try {
        const yamlBody = readProjectFile(projectRootYaml);
        if (yamlBody.length) {
          res
            .status(200)
            .setHeader("Cache-Control", "public, max-age=0")
            .send(yamlBody);
          return;
        }
      } catch (error) {
        Log.error({
          breadcrumbs: ["openapi", "httpAdmin", "get"],
          message: "Could not read configuration",
          properties: {
            projectRootYaml: projectRootYaml,
            error: error,
          },
        });
      }
      res.status(404).send("Configuration node not found");
    });

    RED.httpAdmin.post(projectPathYaml, (req: Request, res: Response): void => {
      Log.debug({
        breadcrumbs: ["openapi", "httpAdmin", "post"],
        message: "Configuration update",
        properties: {
          url: projectPathYaml,
        },
      });
      // Proceed only if not in the main project
      if (orchestratorMainProjectKey === project) {
        res.status(404).send("Invalid project key");
        return;
      }

      if (!req.body) {
        res.status(400).send("Configuration is required");
        return;
      }

      try {
        const data =
          "string" === typeof req.body ? JSON.parse(req.body) : req.body;
        updateYamlConfiguration(project, data);
        res.status(200).send("Configuration was updated");
        return;
      } catch (error) {
        Log.error({
          breadcrumbs: ["openapi", "httpAdmin", "post"],
          message: "Could not update configuration",
          properties: {
            path: projectPathYaml,
            body: req.body,
            error: error,
          },
        });
        res.status(406).send("Configuration is invalid");
      }
    });

    RED.httpAdmin.get(
      `/${project}/swagger-editor/*`,
      (req: Request, res: Response): void => {
        if ("index.html" === req.params[0]) {
          serveIndexFile(
            req,
            res,
            "swagger-editor",
            project,
            project === orchestratorMainProjectKey
          );
        } else {
          serveStaticFile(req, res, "swagger-editor");
        }
      }
    );
  };

  // In none productive mode only
  if (!PROD) {
    RED.httpAdmin.get("/openapi/lib", (req: Request, res: Response): void => {
      req.params[0] = "openapi.js";
      serveStaticFile(req, res, "swagger-editor");
    });

    RED.httpAdmin.get(
      `/openapi/configuration`,
      (req: Request, res: Response): void => {
        res
          .status(200)
          .setHeader("Cache-Control", "public, max-age=3600")
          .send(JSON.stringify(getConfiguration()));
      }
    );

    try {
      const projects = fs.readdirSync(projectsRoot);
      for (const project of projects) {
        const regex = new RegExp("^\\.");
        if (null !== project.match(regex)) continue;
        projectSetterGetterHandler(project);
      }
    } catch (error) {
      Log.error({
        breadcrumbs: ["openapi", "readdirSync", "projectSetterGetterHandler"],
        message: "Projects folder could not be initialized",
        properties: {
          projectsRoot: projectsRoot,
          error: error,
        },
      });
    }
  }

  // In any productive mode
  RED.httpNode.get(
    "/doc/swagger/nls/*",
    (req: Request, res: Response): void => {
      serveStaticFile(req, res, "locales");
    }
  );

  RED.httpNode.get("/doc/swagger/*", (req: Request, res: Response): void => {
    if ("index.html" === req.params[0]) {
      serveIndexFile(
        req,
        res,
        "swagger-editor",
        orchestratorMainProjectKey,
        true
      );
    } else {
      serveStaticFile(req, res, "swagger-editor");
    }
  });

  RED.httpNode.get(mainYamlEndpoint, (req: Request, res: Response): void => {
    const mainProjectRootYaml = `${mainProjectPath}/swagger.yaml`;
    const yamlBody = readProjectFile(mainProjectRootYaml);

    if (yamlBody.length) {
      res
        .status(200)
        .setHeader("Cache-Control", "public, max-age=3600")
        .send(yamlBody);
    } else {
      res.status(404).send("Documentation not found");
    }
  });

  function SwaggerDoc(config: CustomEditorNodeProperties) {
    const node = this;
    RED.nodes.createNode(node, config);
  }

  RED.nodes.registerType("swagger-doc", SwaggerDoc);
};
