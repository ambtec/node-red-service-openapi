<div align="center">
  <a href="https://ambtec.com" title="ambtec" target="_blank">
    <img src="./assets/ambtec-logo-gitlab.png" width="128" />
  </a>
  <p><i>create shape improve</i></p>
</div>

<hr />

# Node Red Module OpenAPI

A module to document and present endpoints via [OpenApi v3](https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.0.0.md) specifications. Supported endpoints are:

- HTTP: Node-Red standard
- SocketIO: https://gitlab.com/ambtec/node-red-module-socketio

## Usage

Install the node module from git:

```
npm install https://gitlab.com/ambtec/node-red-module-openapi
```

## Module integration

Supported nodes provide an OpenAPI edit button like it is shown in this screenshot:

![OpenAPI module support](./assets/Http.png "OpenAPI module support") \
(1. OpenAPI module support)

## OpenAPI configuration

The OpenAPI integration is menat to provide assistance in creation as well as the management of endpoint configurations.

The solution supports Node-Red projects. There for every `` .config.projects.json `` is required:

```
{
    "projects": {
        "orchestrator-flows": {
            ...
        },
        "node-red-flow-main": {
            ...
        },
        "node-red-flow-example-service": {
            ...
        },
        ...
    },
    "activeProject": "orchestrator-flows"
}
```

The `` activeProject `` define the main project which will hold the complete configuration which is collected and merged together of all other projects. The `` activeProject `` is readonly because of the fact, that every project is responsible for it's own configuration. So it won't be overwritten from anywhere else.

Also a project prefix is used to prevent accidential overwrites due similar endpoint namings. There for the project `` package.json `` configuration have to be updated. An new configuration `` openapi-project-route `` is added which determine the project prefix route.

```
{
  ...
  "openapi-project-route": "example",
  ...
}
```

### Panel 1: OpenAPI configuration selection

In this tab a selection is provided with all available OpenAPI endpoints which are configurated at this point of time. On save the configuration of the selected option will be applied to the parent node.

![OpenAPI http selection](./assets/OpenApiConfig.png "OpenAPI http selection") \
(2. OpenAPI http selection)

![OpenAPI socket selection](./assets/HttpSelect.png "OpenAPI socket selection") \
(3. OpenAPI socket selection)

Configuration is applied to parent node on OpenAPU `` update `` event:

![OpenAPI update](./assets/Update.png "OpenAPI update") \
(4. OpenAPI update)

### Panel 2: OpenAPI configuration editor

As OpenAPI Editor the Sagger Editor from [Swagger](https://swagger.io/) is used. This editor offers create performance in creation and mangaging of OpenAPI documentation.

See Swagger [specifications](https://swagger.io/specification/) to learn all about the supported syntax and features.

![OpenAPI configuration selection](./assets/OpenApiEditor.png "OpenAPI configuration selection") \
(5. OpenAPI configuration editor)

Updates to the documentation are stored due the node save event via `` done ``. If you don't save the node all unsaved changes will be lost.

![OpenAPI save](./assets/Done.png "OpenAPI save") \
(6. OpenAPI save)

*Info*: Socket configurations can be best defined as OPTIONS request like in the example below:
```
...
/socket.io<in:testEmit>:
    options:
      summary: options testEmit
      description: ''
      tags:
        - main
      deprecated: false
      parameters:
...
```

Because socket in- and outputs work asynchronous the configuration for incomming topis is defined with `` <in:topic> `` and the configuration for outgoing topics is defined with `` <out:topic> ``.

![OpenAPI socket example](./assets/SocketExample.png "OpenAPI socket example") \
(7. OpenAPI socket example)

Incomming topics are availablr receive socket nodes. Outgoing topics are mapped onto reply socket nodes.

## OpenAPI configuration mapping

The configuration supports http as well as socket in and out messages. Those configuration can be mapped onto a parent node:

- HTTP IN:

![OpenAPI http select](./assets/HttpSelect.png "OpenAPI http select") \
(8. OpenAPI http select)

- SOCKET RECEIVE: `` <in:topic> ``
- SOCKET REPLY: `` <out:topic> ``

![OpenAPI socket select](./assets/SocketSelect.png "OpenAPI socket select") \
(9. OpenAPI socket select)

## OpenAPI presentation

The module offers a public presentation mode. The presentation mode is readonly and allow the consumption, testing and client generation of the available OpenAPI configuration.

The OpenAPI documentation is available on: http://localhost:1880/doc/swagger/index.html

The source is available on: http://localhost:1880/doc/swagger.yaml


## License
MIT

## Credits

- @node-red-node-swagger for inspiration